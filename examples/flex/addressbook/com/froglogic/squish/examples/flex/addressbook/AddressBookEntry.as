package com.froglogic.squish.examples.flex.addressbook
{
    import flash.events.EventDispatcher;
    public class AddressBookEntry extends EventDispatcher
    {
        [Bindable]
        public var fname: String;
        [Bindable]
        public var lname: String;
        [Bindable]
        public var phone: String;
        [Bindable]
        public var email: String;
        public function AddressBookEntry( fname_: String, lname_: String, email_: String, phone_: String ) : void {
            fname = fname_;
            lname = lname_;
            phone = phone_;
            email = email_;
        }
    }
}
