sub invokeMenuItem {
    my ($menu, $item) = @_;
    my $menuName = "{container=':AddressBook.AddressBook_Window' text='$menu' type='MenuBarItem'}";
    mouseClick(waitForObject($menuName));
    mouseClick(waitForObject("{container=$menuName text='$item' type='MenuItem'}"));
}

sub addNameAndAddress {
    my ($forename, $surname, $email, $phone) = @_;
    invokeMenuItem("Edit", "Add...");
    type(waitForObject(":Add Entry_Edit"), $forename);
    type(waitForObject(":Add Entry_Edit"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_2"), $surname);
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_3"), $phone);
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_4"), $email);
    type(waitForObject(":Add Entry_Edit_4"), "<Return>");
}

sub main {
    loadUrl("http://localhost:9000/AddressBook.html");
    my $table = waitForObject(":AddressBook_Table");
    invokeMenuItem("File", "New");
    test::verify($table->rowCount == 0);
    my @data = (["Andy", "Beach", "andy.beach\@nowhere.com", "555 123 6786"],
                ["Candy", "Deane", "candy.deane\@nowhere.com", "555 234 8765"],
                ["Ed", "Fernleaf", "ed.fernleaf\@nowhere.com", "555 876 4654"]);
    foreach $oneNameAndAddress (@data) {
        addNameAndAddress(@{$oneNameAndAddress});
    }
    test::verify($table->rowCount == scalar(@data))    ;
}