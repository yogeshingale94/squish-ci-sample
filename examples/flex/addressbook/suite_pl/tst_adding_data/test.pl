sub invokeMenuItem {
    my ($menu, $item) = @_;
    my $menuName = "{container=':AddressBook.AddressBook_Window' text='$menu' type='MenuBarItem'}";
    mouseClick(waitForObject($menuName));
    mouseClick(waitForObject("{container=$menuName text='$item' type='MenuItem'}"));
}

sub addNameAndAddress {
    my ($forename, $surname, $email, $phone) = @_;
    invokeMenuItem("Edit", "Add...");
    type(waitForObject(":Add Entry_Edit"), $forename);
    type(waitForObject(":Add Entry_Edit"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_2"), $surname);
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_3"), $phone);
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_4"), $email);
    type(waitForObject(":Add Entry_Edit_4"), "<Return>");
}
sub checkNameAndAddress {
    my($table, $record) = @_;
    my @columnNames = testData::fieldNames($record);
    for (my $column = 0; $column <= scalar(@columnNames); $column++) {
        # Application always inserts new entries at the top;
        my $cell = waitForObject("{container=':AddressBook_Table' row='0' column='$column' type='TableCell'}");
        test::compare($cell->text, testData::field($record, $column));
    }
}

sub main {
    loadUrl("http://localhost:9000/AddressBook.html");
    my $table = waitForObject(":AddressBook_Table");
    invokeMenuItem("File", "New");
    test::verify($table->rowCount == 0);
    my $limit = 10; # To avoid testing 100s of rows since that would be boring;
    my @records = testData::dataset("MyAddresses.tsv");
    my $row = 0;
    for (; $row < scalar(@records); ++$row) {
        my $record = $records[$row];
        my $forename = testData::field($record, "Forename");
        my $surname = testData::field($record, "Surname");
        my $email = testData::field($record, "Email");
        my $phone = testData::field($record, "Phone");
        addNameAndAddress($forename, $surname, $email, $phone);
        checkNameAndAddress($table, $record);
        if ($row > $limit ) {
            last;
        }
    }
    test::verify($table->rowCount == $row+1)    ;
}