function invokeMenuItem(menu, item) {
    var menuName = "{container=':AddressBook.AddressBook_Window' text='"+menu+"' type='MenuBarItem'}";
    mouseClick(waitForObject(menuName));
    mouseClick(waitForObject("{container="+menuName+" text='"+item+"' type='MenuItem'}"));
}

function addNameAndAddress(oneNameAndAddress) {
    invokeMenuItem("Edit", "Add...");
    var forename = oneNameAndAddress[0];
    var surname = oneNameAndAddress[1];
    var phone = oneNameAndAddress[3];
    var email = oneNameAndAddress[2];
    type(waitForObject(":Add Entry_Edit"), forename);
    type(waitForObject(":Add Entry_Edit"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_2"), surname);
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_3"), phone);
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_4"), email);
    type(waitForObject(":Add Entry_Edit_4"), "<Return>");
}

function checkNameAndAddress(table, record) {
    for (var column = 0; column < testData.fieldNames(record).length; ++column) {
        // Application always inserts new entries at the top;
        cell = waitForObject("{container=':AddressBook_Table' row='0' column='"+column+"' type='TableCell'}");
        test.compare(cell.text, testData.field(record, column));
    }
}

function main() {
    loadUrl("http://localhost:9000/AddressBook.html");
    var table = waitForObject(":AddressBook_Table");
    invokeMenuItem("File", "New");
    test.verify(table.rowCount == 0);
    var limit = 10 // To avoid testing 100s of rows since that would be boring;
    var records = testData.dataset("MyAddresses.tsv");
    for (var row = 0; row < records.length; ++row) {
        var record = records[row];
        forename = testData.field(record, "Forename");
        surname = testData.field(record, "Surname");
        email = testData.field(record, "Email");
        phone = testData.field(record, "Phone");
        addNameAndAddress(new Array(forename, surname, email, phone));
        checkNameAndAddress(table, record);
        if (row > limit)
            break;
    }
    test.verify(table.rowCount == row+1)
}