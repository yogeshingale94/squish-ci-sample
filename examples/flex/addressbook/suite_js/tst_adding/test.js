function invokeMenuItem(menu, item) {
    var menuName = "{container=':AddressBook.AddressBook_Window' text='"+menu+"' type='MenuBarItem'}";
    mouseClick(waitForObject(menuName));
    mouseClick(waitForObject("{container="+menuName+" text='"+item+"' type='MenuItem'}"));
}

function addNameAndAddress(oneNameAndAddress) {
    invokeMenuItem("Edit", "Add...");
    var forename = oneNameAndAddress[0];
    var surname = oneNameAndAddress[1];
    var phone = oneNameAndAddress[3];
    var email = oneNameAndAddress[2];
    type(waitForObject(":Add Entry_Edit"), forename);
    type(waitForObject(":Add Entry_Edit"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_2"), surname);
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_3"), phone);
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>");
    type(waitForObject(":Add Entry_Edit_4"), email);
    type(waitForObject(":Add Entry_Edit_4"), "<Return>");
}

function main() {
    loadUrl("http://localhost:9000/AddressBook.html");
    var table = waitForObject(":AddressBook_Table");
    invokeMenuItem("File", "New");
    test.verify(table.rowCount == 0);
    var data = new Array(
        new Array("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
        new Array("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
        new Array("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"));
    for (var row = 0; row < data.length; ++row)
        addNameAndAddress(data[row]);
    test.verify(table.rowCount == data.length);
}