
proc main {} {
    invoke loadUrl "http://localhost:9000/AddressBook.html"
    set table [waitForObject ":AddressBook_Table"]
    test compare [property get $table rowCount] 125
    invoke clickButton [waitForObject ":AddressBook.Add_Button"]
    invoke type [waitForObject ":Add Entry_Edit"] "Jane"
    invoke type [waitForObject ":Add Entry_Edit"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_2"] "Doe"
    invoke type [waitForObject ":Add Entry_Edit_2"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_3"] "5551234567"
    invoke type [waitForObject ":Add Entry_Edit_3"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_4"] "jane.doe@nowhere.com"
    invoke clickButton [waitForObject ":Add Entry.Ok_Button"]
    test compare [property get $table rowCount]  126
    invoke mouseClick [waitForObject ":3_0_TableCell"]
    invoke clickButton [waitForObject ":AddressBook.Edit..._Button"]
    invoke mouseClick [waitForObject ":Edit Entry_Edit"] 58 11 [enum MouseButton LeftButton]
    invoke type [waitForObject ":Edit Entry_Edit"] "<Ctrl+a>"
    invoke type [waitForObject ":Edit Entry_Edit"] "Doe"
    invoke clickButton [waitForObject ":Edit Entry.Ok_Button"]
    invoke mouseClick [waitForObject ":1_0_TableCell"]
    invoke clickButton [waitForObject ":AddressBook.Remove_Button"]
    invoke clickButton [waitForObject ":Delete Entry?.OK_Button"]
    test compare [property get $table rowCount] 125
    test compare [property get [waitForObjectExists ":0_2_TableCell"] text] "jane.doe@nowhere.com"
    test compare [property get [waitForObjectExists ":0_3_TableCell"] text] "5551234567"
    test compare [property get [waitForObjectExists ":0_0_TableCell"] text] "Jane"
    test compare [property get [waitForObjectExists ":0_1_TableCell"] text] "Doe"
}