proc invokeMenuItem {menu item} {
    set menuName "{container=':AddressBook.AddressBook_Window' text='$menu' type='MenuBarItem'}"
    invoke mouseClick [waitForObject $menuName]
    invoke mouseClick [waitForObject "{container=$menuName text='$item' type='MenuItem'}"]
}

proc addNameAndAddress {oneNameAndAddress} {
    invokeMenuItem "Edit" "Add..."
    set forename [lindex $oneNameAndAddress 0]
    set surname [lindex $oneNameAndAddress 1]
    set email [lindex $oneNameAndAddress 2]
    set phone [lindex $oneNameAndAddress 3]
    invoke type [waitForObject ":Add Entry_Edit"] $forename
    invoke type [waitForObject ":Add Entry_Edit"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_2"] $surname
    invoke type [waitForObject ":Add Entry_Edit_2"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_3"] $phone
    invoke type [waitForObject ":Add Entry_Edit_3"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_4"] $email
    invoke type [waitForObject ":Add Entry_Edit_4"] "<Return>"
}

proc checkNameAndAddress {table record} {
    set columns [llength [testData fieldNames $record]]
    for {set column 0} {$column < $columns} {incr column} {
        # Application always inserts new entries at the top
        set cell [waitForObject "{container=':AddressBook_Table' row='0' column='$column' type='TableCell'}"]
        test compare [property get $cell text] [testData field $record $column]
    }
}

proc main {} {
    invoke loadUrl "http://localhost:9000/AddressBook.html"
    set table [waitForObject ":AddressBook_Table"]
    invokeMenuItem "File" "New"
    test compare [property get $table rowCount] 0
    set limit 10
    set data [testData dataset "MyAddresses.tsv"]
    set columns [llength [testData fieldNames [lindex $data 0]]]
    set row 0
    for {} {$row < [llength $data]} {incr row} {
        set record [lindex $data $row]
        set forename [testData field $record "Forename"]
        set surname [testData field $record "Surname"]
        set email [testData field $record "Email"]
        set phone [testData field $record "Phone"]
        set details [list $forename $surname $email $phone]
        addNameAndAddress $details
        checkNameAndAddress $table $record
        if {$row > $limit} {
            break
        }
    }
    test compare [property get $table rowCount] [expr $row + 1]    
}
