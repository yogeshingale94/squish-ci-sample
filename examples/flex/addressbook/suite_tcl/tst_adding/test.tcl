proc invokeMenuItem {menu item} {
    set menuName "{container=':AddressBook.AddressBook_Window' text='$menu' type='MenuBarItem'}"
    invoke mouseClick [waitForObject $menuName]
    invoke mouseClick [waitForObject "{container=$menuName text='$item' type='MenuItem'}"]
}

proc addNameAndAddress {oneNameAndAddress} {
    invokeMenuItem "Edit" "Add..."
    set forename [lindex $oneNameAndAddress 0]
    set surname [lindex $oneNameAndAddress 1]
    set email [lindex $oneNameAndAddress 2]
    set phone [lindex $oneNameAndAddress 3]
    invoke type [waitForObject ":Add Entry_Edit"] $forename
    invoke type [waitForObject ":Add Entry_Edit"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_2"] $surname
    invoke type [waitForObject ":Add Entry_Edit_2"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_3"] $phone
    invoke type [waitForObject ":Add Entry_Edit_3"] "<Tab>"
    invoke type [waitForObject ":Add Entry_Edit_4"] $email
    invoke type [waitForObject ":Add Entry_Edit_4"] "<Return>"
}

proc main {} {
    invoke loadUrl "http://localhost:9000/AddressBook.html"
    set table [waitForObject ":AddressBook_Table"]
    invokeMenuItem "File" "New"
    test compare [property get $table rowCount] 0
    set data [list \
            [list "Andy" "Beach" "andy.beach@nowhere.com" "555 123 6786"] \
            [list "Candy" "Deane" "candy.deane@nowhere.com" "555 234 8765"] \
            [list "Ed" "Fernleaf" "ed.fernleaf@nowhere.com" "555 876 4654"] ]
    for {set i 0} {$i < [llength $data]} {incr i} {
        addNameAndAddress [lindex $data $i]
    }
    test compare [property get $table rowCount] [llength $data]    
}