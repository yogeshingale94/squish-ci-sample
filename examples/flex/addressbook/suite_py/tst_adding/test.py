def invokeMenuItem(menu, item):
    menuName = "{container=':AddressBook.AddressBook_Window' text='%s' type='MenuBarItem'}" % menu
    mouseClick(waitForObject(menuName))
    mouseClick(waitForObject("{container=%s text='%s' type='MenuItem'}" % (menuName, item)))

def addNameAndAddress(oneNameAndAddress):
    invokeMenuItem("Edit", "Add...")
    (forename, surname, email, phone) = oneNameAndAddress
    type(waitForObject(":Add Entry_Edit"), forename)
    type(waitForObject(":Add Entry_Edit"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_2"), surname)
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_3"), phone)
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_4"), email)
    type(waitForObject(":Add Entry_Edit_4"), "<Return>")

def main():
    loadUrl("http://localhost:9000/AddressBook.html")
    table = waitForObject(":AddressBook_Table")
    invokeMenuItem("File", "New")
    test.verify(table.rowCount == 0)
    data = [("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
            ("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
            ("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654")]
    for oneNameAndAddress in data:
        addNameAndAddress(oneNameAndAddress)
    test.verify(table.rowCount == len(data))    
