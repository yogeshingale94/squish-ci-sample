def invokeMenuItem(menu, item):
    menuName = "{container=':AddressBook.AddressBook_Window' text='%s' type='MenuBarItem'}" % menu
    mouseClick(waitForObject(menuName))
    mouseClick(waitForObject("{container=%s text='%s' type='MenuItem'}" % (menuName, item)))

def addNameAndAddress(oneNameAndAddress):
    invokeMenuItem("Edit", "Add...")
    (forename, surname, email, phone) = oneNameAndAddress
    type(waitForObject(":Add Entry_Edit"), forename)
    type(waitForObject(":Add Entry_Edit"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_2"), surname)
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_3"), phone)
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_4"), email)
    type(waitForObject(":Add Entry_Edit_4"), "<Return>")

def checkNameAndAddress(table, record):
    for column in range(0, len(testData.fieldNames(record))):
        # Application always inserts new entries at the top
        cell = waitForObject("{container=':AddressBook_Table' row='0' column='%d' type='TableCell'}" % column)
        test.compare(cell.text, testData.field(record, column))

def main():
    loadUrl("http://localhost:9000/AddressBook.html")
    table = waitForObject(":AddressBook_Table")
    invokeMenuItem("File", "New")
    test.verify(table.rowCount == 0)
    limit = 10 # To avoid testing 100s of rows since that would be boring
    for row, record in enumerate(testData.dataset("MyAddresses.tsv")):
        forename = testData.field(record, "Forename")
        surname = testData.field(record, "Surname")
        email = testData.field(record, "Email")
        phone = testData.field(record, "Phone")
        addNameAndAddress((forename, surname, email, phone))
        checkNameAndAddress(table, record)
        if row > limit:
            break
    
    test.verify(table.rowCount == row+1)    
