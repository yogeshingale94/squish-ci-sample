#!/usr/bin/env python

import sys
import BaseHTTPServer
import SimpleHTTPServer

port = 9000
if len(sys.argv) > 1 and sys.argv[1].isdigit():
    port = int(sys.argv[1])
server = BaseHTTPServer.HTTPServer(("127.0.0.1", port),
        SimpleHTTPServer.SimpleHTTPRequestHandler)
try:
    print "[mini-web-server started]"
    server.serve_forever()
except KeyboardInterrupt:
    print "\b\b[mini-web-server stopped]"
