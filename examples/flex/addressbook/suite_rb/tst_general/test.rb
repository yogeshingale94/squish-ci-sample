# encoding: UTF-8
require 'squish'

include Squish

def main
    loadUrl("http://localhost:9000/AddressBook.html")
    table = waitForObject(":AddressBook_Table")
    Test.verify(table.rowCount == 125)
    clickButton(waitForObject(":AddressBook.Add_Button"))
    type(waitForObject(":Add Entry_Edit"), "Jane")
    type(waitForObject(":Add Entry_Edit"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_2"), "Doe")
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_3"), "5551234567")
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_4"), "jane.doe@nowhere.com")
    clickButton(waitForObject(":Add Entry.Ok_Button"))
    Test.verify(table.rowCount == 126)
    mouseClick(waitForObject(":3_0_TableCell"))
    clickButton(waitForObject(":AddressBook.Edit..._Button"))
    mouseClick(waitForObject(":Edit Entry_Edit"), 58, 11, MouseButton::LEFT_BUTTON)
    type(waitForObject(":Edit Entry_Edit"), "<Ctrl+a>")
    type(waitForObject(":Edit Entry_Edit"), "Doe")
    clickButton(waitForObject(":Edit Entry.Ok_Button"))
    mouseClick(waitForObject(":1_0_TableCell"))
    clickButton(waitForObject(":AddressBook.Remove_Button"))
    clickButton(waitForObject(":Delete Entry?.OK_Button"))
    Test.verify(table.rowCount == 125)
    Test.compare(waitForObjectExists(":0_2_TableCell").text, "jane.doe@nowhere.com")
    Test.compare(waitForObjectExists(":0_3_TableCell").text, "5551234567")
    Test.compare(waitForObjectExists(":0_0_TableCell").text, "Jane")
    Test.compare(waitForObjectExists(":0_1_TableCell").text, "Doe")
end
