# encoding: UTF-8
require 'squish'

include Squish

def invokeMenuItem(menu, item)
    menuName = "{container=':AddressBook.AddressBook_Window' text='#{menu}' type='MenuBarItem'}"
    mouseClick(waitForObject(menuName))
    mouseClick(waitForObject("{container=#{menuName} text='#{item}' type='MenuItem'}"))
end

def addNameAndAddress(oneNameAndAddress)
    invokeMenuItem("Edit", "Add...")
    (forename, surname, email, phone) = oneNameAndAddress
    type(waitForObject(":Add Entry_Edit"), forename)
    type(waitForObject(":Add Entry_Edit"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_2"), surname)
    type(waitForObject(":Add Entry_Edit_2"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_3"), phone)
    type(waitForObject(":Add Entry_Edit_3"), "<Tab>")
    type(waitForObject(":Add Entry_Edit_4"), email)
    type(waitForObject(":Add Entry_Edit_4"), "<Return>")
end

def checkNameAndAddress(table, record)
    for column in 0..TestData.fieldNames(record).length-1
        # Application always inserts new entries at the top
        cell = waitForObject("{container=':AddressBook_Table' row='0' column='#{column}' type='TableCell'}")
        Test.compare(cell.text, TestData.field(record, column))
    end
end
  
def main
    loadUrl("http://localhost:9000/AddressBook.html")
    table = waitForObject(":AddressBook_Table")
    invokeMenuItem("File", "New")
    Test.verify(table.rowCount == 0)
    limit = 10 # To avoid testing 100s of rows since that would be boring
    rows = 0
    TestData.dataset("MyAddresses.tsv").each_with_index do
        |record, row|
        forename = TestData.field(record, "Forename")
        surname = TestData.field(record, "Surname")
        email = TestData.field(record, "Email")
        phone = TestData.field(record, "Phone")
        addNameAndAddress([forename, surname, email, phone])
        checkNameAndAddress(table, record)
        rows += 1
        break if row > limit
    end
    Test.compare(table.rowCount, rows)    
end
