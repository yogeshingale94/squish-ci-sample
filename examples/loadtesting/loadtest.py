#!/usr/bin/env python

import os
import datetime
import time

try:
    import thread
except ImportError:
    import _thread as thread

"""
The directory where the script can find squishrunner. And the 
squishrunner executable itself. There should not be any need
to modify the latter.
"""
SQUISH_BIN = "/home/rob/Desktop/squish-20070305-web-linux32/bin/"
SQUISHRUNNER_EXEC = SQUISH_BIN + "squishrunner"

BASEDIR = "/home/rob/stresstesting/"

"""
The testsuite which should be executed with every run
"""
TEST_SUITE = BASEDIR + "suite_stresstest01"

"""
The directory the squish test reports will be written to
"""
REPORTS_DIR = BASEDIR + "reports/"

"""
LOADS_FILE defines the file, the whole list of load averages 
information will be written to
"""
LOADS_FILE = BASEDIR + "loads.tsv"

"""
The information about all squishrunner process will be written to RUNS_FILE
"""
RUNS_FILE = BASEDIR + "runs.tsv"

"""
The host of the webserver for retrieving system informations via SNMP
"""
WEBSERVER_HOST = "localhost"

"""
The hosts that are running the squishservers
"""
SQUISHSERVER_HOSTS = ['10.24.0.100']

"""
How many squishrunner processes do we want to start?
When running the squishrunners the script rotates over the hosts, so
the processes are evenly distributed to the hosts. 
"""
RUN_COUNT = 15

"""
After each start of a squishrunner process we could wait any amount 
of time before starting the next.
"""
RUN_DELAY = 3









"""
The informations related to every squishrunner run will be kept in
SquishrunnerRun objects. 
"""
class SquishrunnerRun:
    id = 0
    starttime = 0
    endtime = 0
    host = ''
    
    def __init__(self, id, host):
        self.id = id
        self.host = host
    
    def duration(self):
        return self.endtime - self.starttime


"""
runSuite(srr) starts a squishrunner instance using the in srr given
host, global set suite and resultsfile; and afterwards sets
start- and endtime in srr
"""
def runSuite(srr):
    srr.starttime = time.time()
    srrCmd = " ".join([SQUISHRUNNER_EXEC, "--host", srr.host,
                       "--testsuite", TEST_SUITE,
                       "--reportgen xml,%s%d.xml" % (REPORTS_DIR, srr.id)])
    os.system(srrCmd)
    srr.endtime = time.time()
    print("call %d finished; needed %s" % (srr.id, srr.endtime - srr.starttime))



"""
we have to cut the load representing float out of the string we got 
from snmpget
"""
def cutAvg(snmpString):
    return snmpString.strip().rsplit("STRING: ", 1)[1]

"""
sysinfo() is called within a separate thread
Every five seconds it fetches the wanted system informations 
by using snmp. At the moment we only retrieve the system load averages.
These informations including a timestamp will be appended to loads.
"""
loads = []
def sysinfo():
    def loadOne(number):
        cmd = "snmpget -v 1 -c commro %s laLoad.%d" % (WEBSERVER_HOST, number)
        tmp = os.popen(cmd, "r")
        reply = tmp.read()
        reply = cutAvg(reply)
        tmp.close()
        return reply

    while 1:
        l1 = loadOne(1)
        l5 = loadOne(2)
        l15 = loadOne(3)
        loads.append({'ts': time.time(), '1m': l1, '5m': l5, '15m': l15})
        time.sleep(5)


"""
The main part of the script starts here.
At first we create a thread for regularly polling the wanted system 
informations
"""
thread.start_new_thread(sysinfo,())

"""
And now start the wanted count of squishrunner processes, rotate over 
the given hosts, and wait RUN_DELAY time before starting the next
"""
runs = []
for i in range(RUN_COUNT):
    tmp = SquishrunnerRun(i, SQUISHSERVER_HOSTS[i % len(SQUISHSERVER_HOSTS)])
    runs.append(tmp)
    thread.start_new_thread(runSuite,(tmp, ))
    time.sleep(RUN_DELAY)


"""
After having started all squishrunners we have to wait until the last one
has finished.
"""
def allRunnersFinished():
    for runner in runs:
        if runner.endtime == 0:
            return False
    return True

while not allRunnersFinished():
    pass


"""
We don't pass here until all squishrunner processes have finished.
And now its time to save the gotten informations.
First the squishrunner calls; second the load averages
"""
fh = open(RUNS_FILE, "w")
for e in runs:
    fh.write("%s\t%s\t%s\n" % (e.id, e.starttime, e.endtime))
fh.close()

fh = open(LOADS_FILE, "w")
for e in loads:
    fh.write("%(ts)s\t%(1m)s\t%(5m)s\t%(15m)s\n" % e)
fh.close()
