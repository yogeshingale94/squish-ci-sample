require 'names';

# encoding: UTF-8
require 'squish'
include Squish

def main
    startBrowser("http://localhost:9090/AddressBook.html")
    confirmPopup(Names::NewButton_button)
    Test.verify(numberOfRows == 0)
    data = [["Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"],
          ["Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"],
          ["Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"]]
    data.each do |oneNameAndAddress|
        addNameAndAddress(oneNameAndAddress)
    end
    Test.compare(numberOfRows, 3)
end

def confirmPopup(button)
    clickButton(waitForObject(button))
    snooze(1.8)
    closeConfirm(Names::ConfirmPopup, true)
end

def numberOfRows
    table = waitForObject(Names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    results = table.evaluateXPath(".//TR[contains(@class,'jqgrow')]")
    results.snapshotLength
end

def addNameAndAddress(oneNameAndAddress)
    clickButton(waitForObject(Names::AddButton_button))
    typeText(waitForObject(Names::Oneitem_forenameEdit_text), oneNameAndAddress[0])
    typeText(waitForObject(Names::Oneitem_surnameEdit_text), oneNameAndAddress[1])
    typeText(waitForObject(Names::Oneitem_emailEdit_text), oneNameAndAddress[2])
    typeText(waitForObject(Names::Oneitem_phoneEdit_text), oneNameAndAddress[3])
    clickButton(waitForObject(Names::Save_button))
end
