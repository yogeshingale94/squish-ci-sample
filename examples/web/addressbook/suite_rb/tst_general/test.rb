# encoding: UTF-8
require 'squish'
require 'names'
include Squish

def main
    startBrowser("http://localhost:9090/AddressBook.html")
    Test.compare(numberOfRows, 125)
    clickButton(waitForObject(Names::AddButton_button))
    typeText(waitForObject(Names::Oneitem_forenameEdit_text), "Jane")
    typeText(waitForObject(Names::Oneitem_surnameEdit_text), "Doe")
    typeText(waitForObject(Names::Oneitem_emailEdit_text), "jane.doe@nowhere.com")
    typeText(waitForObject(Names::Oneitem_phoneEdit_text), "555 123 4567")
    clickButton(waitForObject(Names::Save_button))
    Test.compare(numberOfRows, 126)
    clickButton(waitForObject(Names::Jqg_addresses_3_checkbox))
    clickButton(waitForObject(Names::EditButton_button))
    # manually inserted selectAll() 
    waitForObject(Names::Oneitem_surnameEdit_text).selectAll()
    typeText(waitForObject(Names::Oneitem_surnameEdit_text), "Doe")
    clickButton(waitForObject(Names::Save_button))
    clickButton(waitForObject(Names::Jqg_addresses_1_checkbox))
    clickButton(waitForObject(Names::RemoveButton_button))
    snooze(1.4)
    closeConfirm(Names::ConfirmPopup, true)
    Test.compare(numberOfRows, 125)
    Test.compare(waitForObjectExists(Names::Froglogic_Addressbook_Jane_TD).innerText, "Jane")
    Test.compare(waitForObjectExists(Names::Froglogic_Addressbook_Doe_TD).innerText, "Doe")
    Test.compare(waitForObjectExists(Names::Jane_doe_nowhere_com_TD).innerText, "jane.doe@nowhere.com")
    Test.compare(waitForObjectExists(Names::O555_123_4567_TD).innerText, "555 123 4567")
end

def numberOfRows
    table = waitForObject(Names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    results = table.evaluateXPath(".//TR[contains(@class,'jqgrow')]")
    results.snapshotLength
end
