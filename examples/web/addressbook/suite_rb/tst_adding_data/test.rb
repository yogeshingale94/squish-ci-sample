# encoding: UTF-8
require 'squish'
require 'names';
include Squish

def main
    startBrowser("http://localhost:9090/AddressBook.html")
    confirmPopup(Names::NewButton_button)
    Test.verify(numberOfRows == 0)
    limit = 10
    rows = 0
    TestData.dataset("MyAddresses.tsv").each_with_index do
        |record, row|
        forename = TestData.field(record, "Forename")
        surname = TestData.field(record, "Surname")
        email = TestData.field(record, "Email")
        phone = TestData.field(record, "Phone")
        addNameAndAddress([forename, surname, email, phone]) # pass as an Array
        checkNameAndAddress(record)
        break if row > limit
        rows += 1
    end
    Test.compare(numberOfRows, rows + 1)
end

def confirmPopup(button)
    clickButton(waitForObject(button))
    snooze(1.8)
    closeConfirm(Names::ConfirmPopup, true)
end

def numberOfRows
    table = waitForObject(Names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    results = table.evaluateXPath(".//TR[contains(@class,'jqgrow')]")
    results.snapshotLength
end

def addNameAndAddress(oneNameAndAddress)
    clickButton(waitForObject(Names::AddButton_button))
    typeText(waitForObject(Names::Oneitem_forenameEdit_text), oneNameAndAddress[0])
    typeText(waitForObject(Names::Oneitem_surnameEdit_text), oneNameAndAddress[1])
    typeText(waitForObject(Names::Oneitem_emailEdit_text), oneNameAndAddress[2])
    typeText(waitForObject(Names::Oneitem_phoneEdit_text), oneNameAndAddress[3])
    clickButton(waitForObject(Names::Save_button))
end

def checkNameAndAddress(record)
    table = waitForObject(Names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    cells = [table.evaluateXPath(".//TR/TD[2]"),
           table.evaluateXPath(".//TR/TD[3]"),
           table.evaluateXPath(".//TR/TD[4]"),
           table.evaluateXPath(".//TR/TD[5]")]
    for column in 0...TestData.fieldNames(record).length
        cell = cells[column].snapshotItem(cells[column].snapshotLength-1).innerText
        field = TestData.field(record, column)
        Test.compare(cell, field)
    end
end
