package Names;

use utf8;
use strict;
use warnings;
use Squish::ObjectMapHelper::ObjectName;

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'ObjectMapHelper' module.
#
our $addressbook_html = "AddressBook.html";
our $confirmpopup = "ConfirmPopup";
our $document_html1_body1 = "DOCUMENT.HTML1.BODY1";
our $document_html1_body1_div1_div2_div3_div3_div1_table1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1";
our $document_html1_body1_h11 = "DOCUMENT.HTML1.BODY1.H11";
our $o_window = "[Window]";
our $o555_123_4567_td = {"innerText" => "555 123 4567", "tagName" => "TD"};
our $froglogic_addressbook_browsertab = {"title" => "froglogic Addressbook", "type" => "BrowserTab"};
our $froglogic_addressbook_doe_td = {"container" => $froglogic_addressbook_browsertab, "occurrence" => 2, "tagName" => "TD", "title" => "Doe", "visible" => "true"};
our $froglogic_addressbook_jane_td = {"container" => $froglogic_addressbook_browsertab, "tagName" => "TD", "title" => "Jane", "visible" => "true"};
our $doe_td = {"innerText" => "Doe", "tagName" => "TD"};
our $jane_td = {"innerText" => "Jane", "tagName" => "TD"};
our $remove_submit = {"innerText" => "Remove", "tagName" => "BUTTON", "type" => "submit"};
our $save_button = {"innerText" => "Save", "tagName" => "BUTTON", "type" => "button"};
our $abdu_dickie_grissom_com_td = {"innerText" => "abdu.dickie\@grissom.com", "tagName" => "TD"};
our $ad_crisp_beadsworth_net_td = {"innerText" => "ad.crisp\@beadsworth.net", "tagName" => "TD"};
our $addbutton_button = {"id" => "addButton", "tagName" => "INPUT", "type" => "button", "value" => "Add"};
our $adora_hay_corless_com_td = {"innerText" => "adora.hay\@corless.com", "tagName" => "TD"};
our $editbutton_button = {"id" => "editButton", "tagName" => "INPUT", "type" => "button", "value" => "Edit"};
our $jane_doe_nowhere_com_td = {"innerText" => "jane.doe\@nowhere.com", "tagName" => "TD"};
our $newbutton_button = {"id" => "newButton", "tagName" => "INPUT", "type" => "button", "value" => "New"};
our $oneitem_emailedit_text = {"form" => "oneitem", "id" => "emailEdit", "tagName" => "INPUT", "type" => "text"};
our $oneitem_forenameedit_text = {"form" => "oneitem", "id" => "forenameEdit", "tagName" => "INPUT", "type" => "text"};
our $oneitem_phoneedit_text = {"form" => "oneitem", "id" => "phoneEdit", "tagName" => "INPUT", "type" => "text"};
our $oneitem_surnameedit_text = {"form" => "oneitem", "id" => "surnameEdit", "tagName" => "INPUT", "type" => "text"};
our $removebutton_button = {"id" => "removeButton", "tagName" => "INPUT", "type" => "button", "value" => "Remove"};
our $h2_p_remove_20all_20the_20records_20and_20start_20from_20scratch = "h2><p>Remove%20all%20the%20records%20and%20start%20from%20scratch";
our $jqg_addresses_1_checkbox = {"id" => "jqg_addresses_1", "tagName" => "INPUT", "type" => "checkbox"};
our $jqg_addresses_3_checkbox = {"id" => "jqg_addresses_3", "tagName" => "INPUT", "type" => "checkbox"};
our $jqg_addresses_7_checkbox = {"id" => "jqg_addresses_7", "tagName" => "INPUT", "type" => "checkbox", "value" => "on"};
our $froglogic_addressbook_jane_doe_nowhere_com_td = {"container" => $froglogic_addressbook_browsertab, "tagName" => "TD", "title" => "jane.doe\@nowhere.com", "visible" => "true"};
1;
