require 'names.pl';

sub main
{
    startBrowser("http://localhost:9090/AddressBook.html");
    confirmPopup($Names::newbutton_button);
    test::verify(numberOfRows() == 0);
    my @data = (["Andy", "Beach", "andy.beach\@nowhere.com", "555 123 6786"],
                ["Candy", "Deane", "candy.deane\@nowhere.com", "555 234 8765"],
                ["Ed", "Fernleaf", "ed.fernleaf\@nowhere.com", "555 876 4654"]);
    foreach $oneNameAndAddress (@data) {
        addNameAndAddress(@{$oneNameAndAddress});
    }
    test::compare(numberOfRows(), 3);
}

sub confirmPopup
{
    my ($button) = @_;
    clickButton(waitForObject($button));
    snooze(1.8);
    closeConfirm($Names::confirmpopup, 1);
}

sub numberOfRows
{
    my $table = waitForObject($Names::document_html1_body1_div1_div2_div3_div3_div1_table1);
    my $results = $table->evaluateXPath(".//TR[contains(\@class, 'jqgrow')]");
    return $results->snapshotLength;
}

sub addNameAndAddress
{
    my (@oneNameAndAddress) = @_;
    clickButton(waitForObject($Names::addbutton_button));
    typeText(waitForObject($Names::oneitem_forenameedit_text), $oneNameAndAddress[0]);
    typeText(waitForObject($Names::oneitem_surnameedit_text), $oneNameAndAddress[1]);
    typeText(waitForObject($Names::oneitem_emailedit_text), $oneNameAndAddress[2]);
    typeText(waitForObject($Names::oneitem_phoneedit_text), $oneNameAndAddress[3]);
    clickButton(waitForObject($Names::save_button));
}
