require 'names.pl';

sub main
{
    startBrowser("http://localhost:9090/AddressBook.html");
    confirmPopup($Names::newbutton_button);
    test::verify(numberOfRows() == 0);
    my @records = testData::dataset("MyAddresses.tsv");
    my $limit = 10;
    my $row = 0;
    for (; $row < scalar(@records); ++$row) {
        my $record = $records[$row];
        my $forename = testData::field($record, "Forename");
        my $surname = testData::field($record, "Surname");
        my $email = testData::field($record, "Email");
        my $phone = testData::field($record, "Phone");
        addNameAndAddress(($forename, $surname, $email, $phone));
        checkNameAndAddress($record);
        if ($row > $limit) {
            last;
        }
    }
    test::compare(numberOfRows(), $row + 1);
}

sub checkNameAndAddress
{
    my ($record) = @_;
    my $table = waitForObject($Names::document_html1_body1_div1_div2_div3_div3_div1_table1);
    my @cells = ($table->evaluateXPath(".//TR/TD[2]"),
                 $table->evaluateXPath(".//TR/TD[3]"),
                 $table->evaluateXPath(".//TR/TD[4]"),
                 $table->evaluateXPath(".//TR/TD[5]"));
    my @columnNames = testData::fieldNames($record);
    for (my $column = 0; $column < scalar(@columnNames); ++$column) {
        my $cell = $cells[$column]->snapshotItem($cells[$column]->snapshotLength - 1)->innerText;
        my $field = testData::field($record, $column);
        test::compare($cell, $field);
    }
}

sub confirmPopup
{
    my ($button) = @_;
    clickButton(waitForObject($button));
    snooze(1.8);
    closeConfirm($Names::confirmpopup, 1);
}

sub numberOfRows
{
    my $table = waitForObject($Names::document_html1_body1_div1_div2_div3_div3_div1_table1);
    my $results = $table->evaluateXPath(".//TR[contains(\@class, 'jqgrow')]");
    return $results->snapshotLength;
}

sub addNameAndAddress
{
    my (@oneNameAndAddress) = @_;
    clickButton(waitForObject($Names::addbutton_button));
    typeText(waitForObject($Names::oneitem_forenameedit_text), $oneNameAndAddress[0]);
    typeText(waitForObject($Names::oneitem_surnameedit_text), $oneNameAndAddress[1]);
    typeText(waitForObject($Names::oneitem_emailedit_text), $oneNameAndAddress[2]);
    typeText(waitForObject($Names::oneitem_phoneedit_text), $oneNameAndAddress[3]);
    clickButton(waitForObject($Names::save_button));
}
