require 'names.pl';

sub main
{
    startBrowser("http://localhost:9090/AddressBook.html");
    test::compare(numberOfRows(), 125);
    clickButton(waitForObject($Names::addbutton_button));
    typeText(waitForObject($Names::oneitem_forenameedit_text), "Jane");
    typeText(waitForObject($Names::oneitem_surnameedit_text), "Doe");
    typeText(waitForObject($Names::oneitem_emailedit_text), "jane.doe\@nowhere.com");
    typeText(waitForObject($Names::oneitem_phoneedit_text), "555 123 4567");
    clickButton(waitForObject($Names::save_button));
    test::compare(numberOfRows(), 126);
    clickButton(waitForObject($Names::jqg_addresses_3_checkbox));
    clickButton(waitForObject($Names::editbutton_button));
    # manually inserted selectAll():
    waitForObject($Names::oneitem_surnameedit_text)->selectAll();
    typeText(waitForObject($Names::oneitem_surnameedit_text), "Doe");
    clickButton(waitForObject($Names::save_button));
    clickButton(waitForObject($Names::jqg_addresses_1_checkbox));
    clickButton(waitForObject($Names::removebutton_button));
    snooze(1.4);
    closeConfirm($Names::confirmpopup, 1);
    test::compare(numberOfRows(), 125);
    test::compare(waitForObjectExists($Names::froglogic_addressbook_jane_td)->innerText, "Jane");
    test::compare(waitForObjectExists($Names::froglogic_addressbook_doe_td)->innerText, "Doe");
    test::compare(waitForObjectExists($Names::froglogic_addressbook_jane_doe_nowhere_com_td)->innerText, "jane.doe\@nowhere.com");
    test::compare(waitForObjectExists($Names::o555_123_4567_td)->innerText, "555 123 4567");
}

sub numberOfRows
{
    my $table = waitForObject($Names::document_html1_body1_div1_div2_div3_div3_div1_table1);
    my $results = $table->evaluateXPath(".//TR[contains(\@class, 'jqgrow')]");
    return $results->snapshotLength;
}
