package require squish::objectmaphelper
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

namespace eval ::names {
set ConfirmPopup [::Squish::ObjectName ConfirmPopup]
set DOCUMENT [::Squish::ObjectName DOCUMENT]
set DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1 [::Squish::ObjectName DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1]
set froglogic_Addressbook_BrowserTab [::Squish::ObjectName title {froglogic Addressbook} type BrowserTab]
set froglogic_Addressbook_Save_button [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab innerText Save tagName BUTTON type button]
set froglogic_Addressbook_addButton_button [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab id addButton tagName INPUT type button value Add]
set froglogic_Addressbook_emailEdit_text [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab form oneitem id emailEdit tagName INPUT type text]
set froglogic_Addressbook_forenameEdit_text [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab form oneitem id forenameEdit tagName INPUT type text]
set froglogic_Addressbook_newButton_button [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab id newButton tagName INPUT type button value New]
set froglogic_Addressbook_phoneEdit_text [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab form oneitem id phoneEdit tagName INPUT type text]
set froglogic_Addressbook_surnameEdit_text [::Squish::ObjectName container $froglogic_Addressbook_BrowserTab form oneitem id surnameEdit tagName INPUT type text]
}
