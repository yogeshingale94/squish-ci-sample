source [findFile "scripts" "names.tcl"]

Given "addressbook application is running" {context} {
    invoke startBrowser "http://127.0.0.1:9090/AddressBook.html"
    test compare [property get [waitForObject $names::DOCUMENT] title] "froglogic Addressbook"
}

When "I create a new addressbook" {context} {
    invoke clickButton [waitForObject $names::froglogic_Addressbook_newButton_button]
    snooze 2
    invoke closeConfirm $names::ConfirmPopup true
}

Then "addressbook should have zero entries" {context} {
    test compare [property get [waitForObjectExists $names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1] numChildren] 1
}

When "I add a new person '|word|','|word|','|any|','|integer|' to address book" {context forename surname email phone} {
    invoke clickButton [waitForObject $names::froglogic_Addressbook_addButton_button]
    invoke setText [waitForObject $names::froglogic_Addressbook_forenameEdit_text] $forename
    invoke setText [waitForObject $names::froglogic_Addressbook_surnameEdit_text] $surname
    invoke setText [waitForObject $names::froglogic_Addressbook_emailEdit_text] $email
    invoke setText [waitForObject $names::froglogic_Addressbook_phoneEdit_text] $phone
    invoke clickButton [waitForObject $names::froglogic_Addressbook_Save_button]
    # save userData for last step
    $context userData [dict create forename $forename surname $surname]
}

Then "'|integer|' entries should be present" {context entries} {
    test compare [property get [waitForObjectExists $names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1] numChildren] [expr $entries + 1]
}

When "I add new persons to address book" {context} {
    set table [$context table]
    # Drop initial row with column headers
    foreach row [lreplace $table 0 0] {
        foreach {forename surname email phone} $row break
        invoke clickButton [waitForObject $names::froglogic_Addressbook_addButton_button]
        invoke setText [waitForObject $names::froglogic_Addressbook_forenameEdit_text] $forename
        invoke setText [waitForObject $names::froglogic_Addressbook_surnameEdit_text] $surname
        invoke setText [waitForObject $names::froglogic_Addressbook_emailEdit_text] $email
        invoke setText [waitForObject $names::froglogic_Addressbook_phoneEdit_text] $phone
        invoke clickButton [waitForObject $names::froglogic_Addressbook_Save_button]
    }
}

Then "previously entered forename and surname shall be at the top" {context} {
    test compare [property get [waitForObjectExists "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD2"] innerText] [dict get [$context userData] forename]
    test compare [property get [waitForObjectExists "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD3"] innerText] [dict get [$context userData] surname]
}
