import names

def main():
    startBrowser("http://localhost:9090/AddressBook.html")
    confirmPopup(names.newButton_button)
    test.verify(numberOfRows() == 0, "%d" % numberOfRows())
    data = [("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
            ("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
            ("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654")]
    for oneNameAndAddress in data:
        addNameAndAddress(oneNameAndAddress)
    test.compare(numberOfRows(), 3)

def confirmPopup(button):
    clickButton(waitForObject(button))
    snooze(1.8)
    closeConfirm(names.confirmPopup, True)

def numberOfRows():
    table = waitForObject(names.dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    results = table.evaluateXPath(".//TR[contains(@class, 'jqgrow')]")
    return results.snapshotLength

def addNameAndAddress(oneNameAndAddress):
    clickButton(waitForObject(names.addButton_button))
    typeText(waitForObject(names.oneitem_forenameEdit_text), oneNameAndAddress[0])
    typeText(waitForObject(names.oneitem_surnameEdit_text), oneNameAndAddress[1])
    typeText(waitForObject(names.oneitem_emailEdit_text), oneNameAndAddress[2])
    typeText(waitForObject(names.oneitem_phoneEdit_text), oneNameAndAddress[3])
    clickButton(waitForObject(names.save_button))
