import names

def main():
    startBrowser("http://localhost:9090/AddressBook.html")
    confirmPopup(names.newButton_button)
    test.verify(numberOfRows() == 0)
    limit = 10
    for row, record in enumerate(testData.dataset("MyAddresses.tsv")):
        forename = testData.field(record, "Forename")
        surname = testData.field(record, "Surname")
        email = testData.field(record, "Email")
        phone = testData.field(record, "Phone")
        addNameAndAddress((forename, surname, email, phone)) # pass as a single tuple
        checkNameAndAddress(record)
        if row > limit:
            break
    test.compare(numberOfRows(), row + 1)

def checkNameAndAddress(record):
    table = waitForObject(names.dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    cells = (table.evaluateXPath(".//TR/TD[2]"),
             table.evaluateXPath(".//TR/TD[3]"),
             table.evaluateXPath(".//TR/TD[4]"),
             table.evaluateXPath(".//TR/TD[5]"))
    for column in range(len(testData.fieldNames(record))):
        cell = cells[column].snapshotItem(cells[column].snapshotLength-1).innerText
        field = testData.field(record, column)
        test.compare(cell, field)

def confirmPopup(button):
    clickButton(waitForObject(button))
    snooze(1.8)
    closeConfirm(names.confirmPopup, True)

def numberOfRows():
    table = waitForObject(names.dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    results = table.evaluateXPath(".//TR[contains(@class, 'jqgrow')]")
    return results.snapshotLength

def addNameAndAddress(oneNameAndAddress):
    clickButton(waitForObject(names.addButton_button))
    typeText(waitForObject(names.oneitem_forenameEdit_text), oneNameAndAddress[0])
    typeText(waitForObject(names.oneitem_surnameEdit_text), oneNameAndAddress[1])
    typeText(waitForObject(names.oneitem_emailEdit_text), oneNameAndAddress[2])
    typeText(waitForObject(names.oneitem_phoneEdit_text), oneNameAndAddress[3])
    clickButton(waitForObject(names.save_button))


