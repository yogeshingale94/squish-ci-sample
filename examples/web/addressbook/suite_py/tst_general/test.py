import names

def main():
    startBrowser("http://localhost:9090/AddressBook.html")
    test.compare(numberOfRows(), 125)
    clickButton(waitForObject(names.addButton_button))
    typeText(waitForObject(names.oneitem_forenameEdit_text), "Jane")
    typeText(waitForObject(names.oneitem_surnameEdit_text), "Doe")
    typeText(waitForObject(names.oneitem_emailEdit_text), "jane.doe@nowhere.com")
    typeText(waitForObject(names.oneitem_phoneEdit_text), "555 123 4567")
    clickButton(waitForObject(names.save_button))
    test.compare(numberOfRows(), 126)
    clickButton(waitForObject(names.jqg_addresses_3_checkbox))
    clickButton(waitForObject(names.editButton_button))
    # manually inserted selectAll():
    waitForObject(names.oneitem_surnameEdit_text).selectAll()
    typeText(waitForObject(names.oneitem_surnameEdit_text), "Doe")
    clickButton(waitForObject(names.save_button))
    clickButton(waitForObject(names.jqg_addresses_1_checkbox))
    clickButton(waitForObject(names.removeButton_button))
    snooze(1.4)
    closeConfirm(names.confirmPopup, True)
    test.compare(numberOfRows(), 125)
    test.compare(waitForObjectExists(names.froglogic_Addressbook_Jane_TD).innerText, "Jane")
    test.compare(waitForObjectExists(names.froglogic_Addressbook_Doe_TD).innerText, "Doe")
    test.compare(waitForObjectExists(names.jane_doe_nowhere_com_TD).innerText, "jane.doe@nowhere.com")
    test.compare(waitForObjectExists(names.o555_123_4567_TD).innerText, "555 123 4567")

def numberOfRows():
    table = waitForObject(names.dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1)
    results = table.evaluateXPath(".//TR[contains(@class, 'jqgrow')]")
    return results.snapshotLength
