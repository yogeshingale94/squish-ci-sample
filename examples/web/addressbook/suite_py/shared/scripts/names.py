# encoding: UTF-8

from objectmaphelper import *

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

addressBook_html = "AddressBook.html"
confirmPopup = "ConfirmPopup"
dOCUMENT_HTML1_BODY1 = "DOCUMENT.HTML1.BODY1"
dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1"
dOCUMENT_HTML1_BODY1_H11 = "DOCUMENT.HTML1.BODY1.H11"
o_Window = "[Window]"
o555_123_4567_TD = {"innerText": "555 123 4567", "tagName": "TD"}
froglogic_Addressbook_BrowserTab = {"title": "froglogic Addressbook", "type": "BrowserTab"}
froglogic_Addressbook_Doe_TD = {"container": froglogic_Addressbook_BrowserTab, "occurrence": 2, "tagName": "TD", "title": "Doe", "visible": True}
froglogic_Addressbook_Jane_TD = {"container": froglogic_Addressbook_BrowserTab, "tagName": "TD", "title": "Jane", "visible": True}
doe_TD = {"innerText": "Doe", "tagName": "TD"}
jane_TD = {"innerText": "Jane", "tagName": "TD"}
remove_submit = {"innerText": "Remove", "tagName": "BUTTON", "type": "submit"}
save_button = {"innerText": "Save", "tagName": "BUTTON", "type": "button"}
abdu_dickie_grissom_com_TD = {"innerText": "abdu.dickie@grissom.com", "tagName": "TD"}
ad_crisp_beadsworth_net_TD = {"innerText": "ad.crisp@beadsworth.net", "tagName": "TD"}
addButton_button = {"id": "addButton", "tagName": "INPUT", "type": "button", "value": "Add"}
adora_hay_corless_com_TD = {"innerText": "adora.hay@corless.com", "tagName": "TD"}
editButton_button = {"id": "editButton", "tagName": "INPUT", "type": "button", "value": "Edit"}
jane_doe_nowhere_com_TD = {"innerText": "jane.doe@nowhere.com", "tagName": "TD"}
newButton_button = {"id": "newButton", "tagName": "INPUT", "type": "button", "value": "New"}
oneitem_emailEdit_text = {"form": "oneitem", "id": "emailEdit", "tagName": "INPUT", "type": "text"}
oneitem_forenameEdit_text = {"form": "oneitem", "id": "forenameEdit", "tagName": "INPUT", "type": "text"}
oneitem_phoneEdit_text = {"form": "oneitem", "id": "phoneEdit", "tagName": "INPUT", "type": "text"}
oneitem_surnameEdit_text = {"form": "oneitem", "id": "surnameEdit", "tagName": "INPUT", "type": "text"}
removeButton_button = {"id": "removeButton", "tagName": "INPUT", "type": "button", "value": "Remove"}
h2_p_Remove_20all_20the_20records_20and_20start_20from_20scratch = "h2><p>Remove%20all%20the%20records%20and%20start%20from%20scratch"
jqg_addresses_1_checkbox = {"id": "jqg_addresses_1", "tagName": "INPUT", "type": "checkbox"}
jqg_addresses_3_checkbox = {"id": "jqg_addresses_3", "tagName": "INPUT", "type": "checkbox"}
jqg_addresses_7_checkbox = {"id": "jqg_addresses_7", "tagName": "INPUT", "type": "checkbox", "value": "on"}
