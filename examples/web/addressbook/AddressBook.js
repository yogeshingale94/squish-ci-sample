function setupTable() {
    $("[id='addresses']").jqGrid({
        datatyppe: "local",
        colNames: ["Forename", "Surname", "Email", "Phone"],
        colModel: [
            {name:'FORENAME', index:'FORENAME', sortable:false},
            {name:'SURNAME', index:'SURNAME', sortable:false},
            {name:'EMAIL', index:'EMAIL', sortable:false},
            {name:'PHONE', index:'PHONE', sortable:false}
        ],
        multiselect: true,
        autowidth: true,
        height: 350,
        onSelectRow: function(rowid, status) {
            var selectedEntries = getSelectedRowIds();
            $("#removeButton").button( "option", "disabled", !( selectedEntries.length > 0 ) );
            $("#editButton").button( "option", "disabled", !( selectedEntries.length==1 ) );
        }
    });
    fillTable(addresses);
}
function getSelectedRowIds() {
    // Create a copy of the selected entries, this is necessary since selarrrow returns a reference
    // to the internal data and removing a row may change this internal array. Hence we need a copy to
    // iterate over it.
    var grid = $("[id='addresses']");
    return jQuery.makeArray(grid.jqGrid("getGridParam", "selarrrow"));
}
function showError( txt ) {
    $(".validateTips").text( txt ).addClass("ui-state-highlight");
    setTimeout(function(){
        $(".validateTips").removeClass("ui-state-highlight", 1500)
    }, 500);
}
function checkNonEmpty( obj, name ) {
    if( !obj.val() || obj.val().length == 0 ) {
        obj.addClass("ui-state-error");
        showError( name + " must not be empty!");
        return false;
    }
    return true;
}
function checkRegEx( obj, pattern, errTxt ) {
    if( !( pattern.test( obj.val() ) ) ) {
        obj.addClass("ui-state-error");
        showError( errTxt )
        return false;
    }
    return true;
}
function addEntryToTable( hiddenid, forenameval, surnameval, emailval, phoneval ) {
    if( !hiddenid || hiddenid.length == 0 ) {
        var knownIds = $("#addresses").jqGrid("getDataIDs");
        // We always append at the moment
        knownIds.sort();
        var newid = 0;
        if( knownIds.length > 0 ) {
            newid = parseInt(knownIds[knownIds.length-1]) + 1;
        }
        $("table[id='addresses']").jqGrid("addRowData", newid, {
            FORENAME:forenameval,
            SURNAME:surnameval,
            EMAIL:emailval,
            PHONE:phoneval,
        });
    } else {
        $("table[id='addresses']").jqGrid("setRowData", hiddenid, {
            FORENAME:forenameval,
            SURNAME:surnameval,
            EMAIL:emailval,
            PHONE:phoneval,
        });
    }
}
function setupErrorDialog() {
    $("#errordlg").dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            Ok : function() {
                $(this).dialog( "close" );
            }
        }
    });
}
function setupDialog() {
    var forename = $("#forenameEdit");
    var surname = $("#surnameEdit");
    var email = $("#emailEdit");
    var phone = $("#phoneEdit");
    var hiddenid = $("#hiddenid");
    var allFields = $([]).add(forename).add(surname).add(email).add(phone).add(hiddenid);
    $("[id='addressform']").dialog({
        autoOpen: false,
        modal: true,
        // Need a bit larger min width for firefox
        minWidth: 350,
        buttons: {
            "Save" : function() {
                var EmailPattern = /^[\w.]+@[\w]+\.[\w]{1,4}$/;
                var PhonePattern = /^[\d\s().]{7,}$/;
                allFields.removeClass("ui-state-error");
                var bValid = true;
                bValid = bValid && checkNonEmpty( forename, "Forename" );
                bValid = bValid && checkNonEmpty( surname, "Surname" );
                // Phone and email are allowed to be empty, but if they're not they need to be checked
                bValid = bValid && ( !email.val() || email.val().length == 0 || checkRegEx( email, EmailPattern, "Email must be of the form user@domain, eg. squish@froglogic.com!" ) );
                bValid = bValid && ( !phone.val() || phone.val().length == 0 || checkRegEx( phone, PhonePattern, "Phone must consist of at least numbers, space, dot or parenthesis!" ) );
                if( bValid ) {
                    addEntryToTable( hiddenid.val(), forename.val(), surname.val(), email.val(), phone.val() );
                    allFields.val("");
                    $("#addresses").jqGrid("resetSelection");
                    $(this).dialog( "close" );
                }
            },
            Cancel: function() {
                $(this).dialog("close");
            }
        },
        close: function() {
            allFields.val("");
            $("#fornameEdit").removeClass( "ui-state-error" );
        }
    });
}
function fillTable(listOfAddresses) {
    // Adds all data from the parameter assuming each entry has an ID property denoting the row-id
    $("table[id='addresses']").jqGrid("addRowData", "ID", listOfAddresses);
}

function addEntry() {
    $("[id='addressform']").dialog("open");
}
function editSelectedEntry() {
    var selectedEntries = getSelectedRowIds();
    if( selectedEntries.length > 1 || selectedEntries.length == 0 ) {
        $("#errordlg").dialog("open");
        return;
    }
    $("#hiddenid").val(selectedEntries[0]);
    var rowdata = $("#addresses").jqGrid("getRowData", selectedEntries[0]);
    $("#forenameEdit").val(rowdata["FORENAME"]);
    $("#surnameEdit").val(rowdata["SURNAME"]);
    $("#emailEdit").val(rowdata["EMAIL"]);
    $("#phoneEdit").val(rowdata["PHONE"]);
    $("[id='addressform']").dialog("open");
}
function removeSelectedEntries() {
    var grid = $("[id='addresses']");
    var selectedEntries = getSelectedRowIds();
    for( var i = 0; i < selectedEntries.length; ++i ) {
        grid.jqGrid("delRowData", selectedEntries[i] );
    }
}
function clearTable() {
    $("table[id='addresses']").jqGrid("clearGridData");
}

function setupButtons() {
    $('[id="newButton"]').click(function() {
        // Use a standard confirm box here for the testsuite's usage
        if( confirm( "Remove all Entries?" ) ) {
            clearTable();
        }
    }).button();
    $('[id="addButton"]').click(function() {
        addEntry();
    }).button();
    $('[id="editButton"]').click(function() {
        editSelectedEntry();
    }).button({
        disabled: true
    });
    $('[id="removeButton"]').click(function() {
        if( confirm("Remove selected rows?") ) {
            removeSelectedEntries();
        }
    }).button({
        disabled: true
    });
}
