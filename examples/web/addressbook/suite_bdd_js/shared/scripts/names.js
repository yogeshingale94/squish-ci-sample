// See the chapter 'Script-Based Object Map API' in the Squish manual for
// documentation of the functionality offered by the 'objectmaphelper' module.
import { RegularExpression, Wildcard } from 'objectmaphelper.js';

export var confirmPopup = "ConfirmPopup";
export var dOCUMENT = "DOCUMENT";
export var dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1TBODY1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1";
export var froglogicAddressbookBrowserTab = {"title": "froglogic Addressbook", "type": "BrowserTab"};
export var froglogicAddressbookSaveButton = {"container": froglogicAddressbookBrowserTab, "innerText": "Save", "tagName": "BUTTON", "type": "button"};
export var froglogicAddressbookAddButtonButton = {"container": froglogicAddressbookBrowserTab, "id": "addButton", "tagName": "INPUT", "type": "button", "value": "Add"};
export var froglogicAddressbookEmailEditText = {"container": froglogicAddressbookBrowserTab, "form": "oneitem", "id": "emailEdit", "tagName": "INPUT", "type": "text"};
export var froglogicAddressbookForenameEditText = {"container": froglogicAddressbookBrowserTab, "form": "oneitem", "id": "forenameEdit", "tagName": "INPUT", "type": "text"};
export var froglogicAddressbookNewButtonButton = {"container": froglogicAddressbookBrowserTab, "id": "newButton", "tagName": "INPUT", "type": "button", "value": "New"};
export var froglogicAddressbookPhoneEditText = {"container": froglogicAddressbookBrowserTab, "form": "oneitem", "id": "phoneEdit", "tagName": "INPUT", "type": "text"};
export var froglogicAddressbookSurnameEditText = {"container": froglogicAddressbookBrowserTab, "form": "oneitem", "id": "surnameEdit", "tagName": "INPUT", "type": "text"};
