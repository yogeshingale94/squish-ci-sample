import * as names from 'names.js';

Given("addressbook application is running", function(context) {
    startBrowser("http://127.0.0.1:9090/AddressBook.html");
    test.compare(waitForObjectExists(names.dOCUMENT).title, "froglogic Addressbook");
});

When("I create a new addressbook", function(context) {
    clickButton(waitForObject(names.froglogicAddressbookNewButtonButton));
    snooze(2);
    closeConfirm(names.confirmPopup, true);
});

Then("addressbook should have zero entries", function(context) {
    test.compare(waitForObjectExists(names.dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1TBODY1).numChildren, 1);
});

When("I add a new person '|word|','|word|','|any|','|integer|' to address book",
    function(context, forename, surname, email, phone) {
    clickButton(waitForObject(names.froglogicAddressbookAddButtonButton));
    mouseClick(waitForObject(names.froglogicAddressbookAddButtonButton));
    setText(waitForObject(names.froglogicAddressbookForenameEditText), forename);
    setText(waitForObject(names.froglogicAddressbookSurnameEditText), surname);
    setText(waitForObject(names.froglogicAddressbookEmailEditText), email);
    setText(waitForObject(names.froglogicAddressbookPhoneEditText), phone);
    clickButton(waitForObject(names.froglogicAddressbookSaveButton));
    // save userData for last step
    context.userData["forename"] = forename;
    context.userData["surname"] = surname;
});

Then("'|integer|' entries should be present", function(context, entries) {
    test.compare(waitForObjectExists(names.dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1TBODY1).numChildren, entries + 1);
});

When("I add new persons to address book", function(context) {
    var table = context.table;
    // Skip initial row with column headers by starting at index 1
    for (var i = 1; i < table.length; ++i) {
        var forename = table[i][0];
        var surname = table[i][1];
        var email = table[i][2];
        var phone = table[i][3];
        clickButton(waitForObject(names.froglogicAddressbookAddButtonButton));
        mouseClick(waitForObject(names.froglogicAddressbookAddButtonButton));
        setText(waitForObject(names.froglogicAddressbookForenameEditText), forename);
        setText(waitForObject(names.froglogicAddressbookSurnameEditText), surname);
        setText(waitForObject(names.froglogicAddressbookEmailEditText), email);
        setText(waitForObject(names.froglogicAddressbookPhoneEditText), phone);
        clickButton(waitForObject(names.froglogicAddressbookSaveButton));
    }
});

Then("previously entered forename and surname shall be at the top", function(context) {
    test.compare(waitForObjectExists("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD2").innerText,
        context.userData["forename"], "forename?");
    test.compare(waitForObjectExists("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD3").innerText,
        context.userData["surname"], "surname?");
});
