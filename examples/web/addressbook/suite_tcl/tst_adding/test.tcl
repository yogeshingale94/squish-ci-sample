source [findFile "scripts" "names.tcl"]


proc main {} {
    invoke startBrowser "http://localhost:9090/AddressBook.html"
    confirmPopup $names::newButton_button
    test compare [numberOfRows] 0
    set data [list \
        [list "Andy" "Beach" "andy.beach@nowhere.com" "555 123 6786"] \
        [list "Candy" "Deane" "candy.deane@nowhere.com" "555 234 8765"] \
        [list "Ed" "Fernleaf" "ed.fernleaf@nowhere.com" "555 876 4654"] ]
    for {set i 0} {$i < [llength $data]} {incr i} {
        addNameAndAddress [lindex $data $i]
    }
    test compare [numberOfRows] 3
}

proc confirmPopup {button} {
    invoke clickButton [waitForObject $button]
    snooze 1.8
    invoke closeConfirm $names::ConfirmPopup true
}

proc numberOfRows {} {
    set table [waitForObject \
        $names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1]
    set results [invoke $table evaluateXPath {.//TR[contains(@class,'jqgrow')]}]
    return [property get $results snapshotLength]
}

proc addNameAndAddress {oneNameAndAddress} {
    invoke clickButton [waitForObject $names::addButton_button]
    invoke typeText [waitForObject $names::oneitem_forenameEdit_text] [lindex $oneNameAndAddress 0]
    invoke typeText [waitForObject $names::oneitem_surnameEdit_text] [lindex $oneNameAndAddress 1]
    invoke typeText [waitForObject $names::oneitem_emailEdit_text] [lindex $oneNameAndAddress 2]
    invoke typeText [waitForObject $names::oneitem_phoneEdit_text] [lindex $oneNameAndAddress 3]
    invoke clickButton [waitForObject $names::Save_button]
}
