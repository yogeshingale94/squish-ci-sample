source [findFile "scripts" "names.tcl"]

proc main {} {
    invoke startBrowser "http://localhost:9090/AddressBook.html"
    confirmPopup $names::newButton_button
    test compare [numberOfRows] 0
    set limit 10
    set data [testData dataset "MyAddresses.tsv"]
    set columns [llength [testData fieldNames [lindex $data 0]]]
    set row 0
    for {} {$row < [llength $data]} {incr row} {
        set record [lindex $data $row]
        set forename [testData field $record "Forename"]
        set surname [testData field $record "Surname"]
        set email [testData field $record "Email"]
        set phone [testData field $record "Phone"]
        set details [list $forename $surname $email $phone]
        addNameAndAddress $details
        checkNameAndAddress $record
        if {$row > $limit} {
            break
        }
    }
    test compare [numberOfRows] [expr $row + 1]
}

proc checkNameAndAddress {record} {
    set table [waitForObject $names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1]
    set cells [list \
        [invoke $table evaluateXPath {.//TR/TD[2]}] \
        [invoke $table evaluateXPath {.//TR/TD[3]}] \
        [invoke $table evaluateXPath {.//TR/TD[4]}] \
        [invoke $table evaluateXPath {.//TR/TD[5]}]]
    set columns [llength [testData fieldNames $record]]
    for {set column 0} {$column < $columns} {incr column} {
        set itemPos [expr [property get [lindex $cells $column] snapshotLength ] - 1 ]
        set cell [property get [invoke [lindex $cells $column] \
            snapshotItem $itemPos] innerText]
        set field [testData field $record $column]
        test compare $cell $field
    }
}

proc confirmPopup {button} {
    invoke clickButton [waitForObject $button]
    snooze 1.8
    invoke closeConfirm $names::ConfirmPopup true
}

proc numberOfRows {} {
    set table [waitForObject \
        $names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1]
    set results [invoke $table evaluateXPath {.//TR[contains(@class,'jqgrow')]}]
    return [property get $results snapshotLength]
}

proc addNameAndAddress {oneNameAndAddress} {
    invoke clickButton [waitForObject $names::addButton_button]
    invoke typeText [waitForObject $names::oneitem_forenameEdit_text] [lindex $oneNameAndAddress 0]
    invoke typeText [waitForObject $names::oneitem_surnameEdit_text] [lindex $oneNameAndAddress 1]
    invoke typeText [waitForObject $names::oneitem_emailEdit_text] [lindex $oneNameAndAddress 2]
    invoke typeText [waitForObject $names::oneitem_phoneEdit_text] [lindex $oneNameAndAddress 3]
    invoke clickButton [waitForObject $names::Save_button]
}
