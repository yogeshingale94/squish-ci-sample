source [findFile "scripts" "names.tcl"]


proc main {} {
    invoke startBrowser "http://localhost:9090/AddressBook.html"
    test compare [numberOfRows] 125
    invoke clickButton [waitForObject $names::addButton_button]
    invoke typeText [waitForObject $names::oneitem_forenameEdit_text] "Jane"
    invoke typeText [waitForObject $names::oneitem_surnameEdit_text] "Doe"
    invoke typeText [waitForObject $names::oneitem_emailEdit_text] "jane.doe@nowhere.com"
    invoke typeText [waitForObject $names::oneitem_phoneEdit_text] "555 123 4567"
    invoke clickButton [waitForObject $names::Save_button]
    test compare [numberOfRows] 126
    invoke clickButton [waitForObject $names::jqg_addresses_3_checkbox]
    invoke clickButton [waitForObject $names::editButton_button]
    # manually inserted selectAll:
    invoke [waitForObject $names::oneitem_surnameEdit_text] selectAll
    invoke typeText [waitForObject $names::oneitem_surnameEdit_text] "Doe"
    invoke clickButton [waitForObject $names::Save_button]
    invoke clickButton [waitForObject $names::jqg_addresses_1_checkbox]
    invoke clickButton [waitForObject $names::removeButton_button]
    snooze 1.4
    invoke closeConfirm $names::ConfirmPopup true
    test compare [numberOfRows] 125
    test compare [property get [waitForObjectExists $names::froglogic_Addressbook_Jane_TD] innerText] "Jane"
    test compare [property get [waitForObjectExists $names::froglogic_Addressbook_Doe_TD] innerText] "Doe"
    test compare [property get [waitForObjectExists $names::jane_doe_nowhere_com_TD] innerText] "jane.doe@nowhere.com"
    test compare [property get [waitForObjectExists $names::555_123_4567_TD] innerText] "555 123 4567"

}

proc numberOfRows {} {
    set table [waitForObject \
        $names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1]
    set results [invoke $table evaluateXPath {.//TR[contains(@class,'jqgrow')]}]
    return [property get $results snapshotLength]
}
