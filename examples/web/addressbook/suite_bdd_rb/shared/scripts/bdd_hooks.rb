# encoding: UTF-8

# This file contains hook functions to run as the .feature file is executed.
#
# A common use-case is to use the OnScenarioStart/OnScenarioEnd hooks to
# start and stop an AUT, e.g.
#
# OnScenarioStart do |context|
#     Squish::startApplication 'addressbook'
# end
#
# OnScenarioEnd do |context|
#     Squish::currentApplicationContext.detach
# end
#
# See the section 'Performing Actions During Test Execution Via Hooks' in the Squish
# manual for a complete reference of the available API.
#

require 'squish'

include Squish

# Detach (i.e. potentially terminate) all AUTs at the end of a scenario
OnScenarioEnd do |context|
    closeWindow(":[Window]");
end

