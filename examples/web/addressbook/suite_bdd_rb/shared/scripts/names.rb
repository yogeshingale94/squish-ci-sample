# encoding: UTF-8

require 'squish/objectmaphelper'
# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

module Names

include Squish::ObjectMapHelper

ConfirmPopup = "ConfirmPopup"
DOCUMENT = "DOCUMENT"
DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1"
Froglogic_Addressbook_BrowserTab = {:title => "froglogic Addressbook", :type => "BrowserTab"}
Froglogic_Addressbook_Save_button = {:container => Froglogic_Addressbook_BrowserTab, :innerText => "Save", :tagName => "BUTTON", :type => "button"}
Froglogic_Addressbook_addButton_button = {:container => Froglogic_Addressbook_BrowserTab, :id => "addButton", :tagName => "INPUT", :type => "button", :value => "Add"}
Froglogic_Addressbook_emailEdit_text = {:container => Froglogic_Addressbook_BrowserTab, :form => "oneitem", :id => "emailEdit", :tagName => "INPUT", :type => "text"}
Froglogic_Addressbook_forenameEdit_text = {:container => Froglogic_Addressbook_BrowserTab, :form => "oneitem", :id => "forenameEdit", :tagName => "INPUT", :type => "text"}
Froglogic_Addressbook_newButton_button = {:container => Froglogic_Addressbook_BrowserTab, :id => "newButton", :tagName => "INPUT", :type => "button", :value => "New"}
Froglogic_Addressbook_phoneEdit_text = {:container => Froglogic_Addressbook_BrowserTab, :form => "oneitem", :id => "phoneEdit", :tagName => "INPUT", :type => "text"}
Froglogic_Addressbook_surnameEdit_text = {:container => Froglogic_Addressbook_BrowserTab, :form => "oneitem", :id => "surnameEdit", :tagName => "INPUT", :type => "text"}
end
