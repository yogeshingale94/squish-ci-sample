# encoding: UTF-8

require 'names';
include Squish

Given("addressbook application is running") do |context|
    startBrowser("http://127.0.0.1:9090/AddressBook.html")
    Test.compare(waitForObjectExists(Names::DOCUMENT).title, "froglogic Addressbook")
end

When("I create a new addressbook") do |context|
    clickButton(waitForObject(Names::Froglogic_Addressbook_newButton_button))
    snooze(2)
    closeConfirm(Names::ConfirmPopup, true)
end

Then("addressbook should have zero entries") do |context|
    Test.compare(waitForObjectExists(Names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1).numChildren, 1)
end

When("I add a new person '|word|','|word|','|any|','|integer|' to address book") do |context, forename, surname, email, phone|
    clickButton(waitForObject(Names::Froglogic_Addressbook_addButton_button))
    setText(waitForObject(Names::Froglogic_Addressbook_forenameEdit_text), forename)
    setText(waitForObject(Names::Froglogic_Addressbook_surnameEdit_text), surname)
    setText(waitForObject(Names::Froglogic_Addressbook_emailEdit_text), email)
    setText(waitForObject(Names::Froglogic_Addressbook_phoneEdit_text), phone)
    clickButton(waitForObject(Names::Froglogic_Addressbook_Save_button))
    # save userData for last step
    context.userData = Hash.new
    context.userData[:forename] = forename
    context.userData[:surname] = surname
end

Then("'|integer|' entries should be present") do |context, entries|
    Test.compare(waitForObjectExists(Names::DOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1).numChildren, entries + 1)
end

When("I add new persons to address book") do |context|
    table = context.table
    # Drop initial row with column headers
    table.shift
    for forename, surname, email, phone in table do
        clickButton(waitForObject(Names::Froglogic_Addressbook_addButton_button))
        setText(waitForObject(Names::Froglogic_Addressbook_forenameEdit_text), forename)
        setText(waitForObject(Names::Froglogic_Addressbook_surnameEdit_text), surname)
        setText(waitForObject(Names::Froglogic_Addressbook_emailEdit_text), email)
        setText(waitForObject(Names::Froglogic_Addressbook_phoneEdit_text), phone)
        clickButton(waitForObject(Names::Froglogic_Addressbook_Save_button))
    end
end

Then("previously entered forename and surname shall be at the top") do |context|
    Test.compare(waitForObjectExists("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD2").innerText,
        context.userData[:forename], "forename?")
    Test.compare(waitForObjectExists("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD3").innerText,
        context.userData[:surname], "surname?")
end

