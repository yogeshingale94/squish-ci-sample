# -*- coding: utf-8 -*-

import names

@Given("addressbook application is running")
def step(context):
    startBrowser("http://127.0.0.1:9090/AddressBook.html")
    test.compare(waitForObjectExists(names.dOCUMENT).title, "froglogic Addressbook")

@When("I create a new addressbook")
def step(context):
    clickButton(waitForObject(names.froglogic_Addressbook_newButton_button))
    snooze(2)
    closeConfirm(names.confirmPopup, True)

@Then("addressbook should have zero entries")
def step(context):
    test.compare(waitForObjectExists(names.dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1).numChildren, 1)

@When("I add a new person '|word|','|word|','|any|','|integer|' to address book")
def step(context, forename, surname, email, phone):
    clickButton(waitForObject(names.froglogic_Addressbook_addButton_button))
    setText(waitForObject(names.froglogic_Addressbook_forenameEdit_text), forename)
    setText(waitForObject(names.froglogic_Addressbook_surnameEdit_text), surname)
    setText(waitForObject(names.froglogic_Addressbook_emailEdit_text), email)
    setText(waitForObject(names.froglogic_Addressbook_phoneEdit_text), phone)
    clickButton(waitForObject(names.froglogic_Addressbook_Save_button))
    # save userData for last step
    context.userData = {}
    context.userData['forename'] = forename
    context.userData['surname'] = surname

@Then("'|integer|' entries should be present")
def step(context, entries):
    test.compare(waitForObjectExists(names.dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1).numChildren, entries + 1)

@When("I add new persons to address book")
def step(context):
    table = context.table
    # Drop initial row with column headers
    table.pop(0)
    for (forename, surname, email, phone) in table:
        clickButton(waitForObject(names.froglogic_Addressbook_addButton_button))
        setText(waitForObject(names.froglogic_Addressbook_forenameEdit_text), forename)
        setText(waitForObject(names.froglogic_Addressbook_surnameEdit_text), surname)
        setText(waitForObject(names.froglogic_Addressbook_emailEdit_text), email)
        setText(waitForObject(names.froglogic_Addressbook_phoneEdit_text), phone)
        clickButton(waitForObject(names.froglogic_Addressbook_Save_button))

@Then("previously entered forename and surname shall be at the top")
def step(context):
    test.compare(waitForObjectExists("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD2").innerText,
                 context.userData['forename'])
    test.compare(waitForObjectExists("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD3").innerText,
                 context.userData['surname'])
# eof
