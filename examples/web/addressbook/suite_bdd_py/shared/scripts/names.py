# encoding: UTF-8

from objectmaphelper import *

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'objectmaphelper' module.

bobDoeBob_m_com123321231_DIV = {"innerText": "BobDoeBob@m.com123321231", "occurrence": 2, "tagName": "DIV"}
bob_TD = {"tagName": "TD", "title": "Bob"}
confirmPopup = "ConfirmPopup"
dOCUMENT = "DOCUMENT"
dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV2_DIV1_TABLE1_THEAD1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV2.DIV1.TABLE1.THEAD1"
dOCUMENT_HTML1_BODY1_DIV1_DIV2_DIV3_DIV3_DIV1_TABLE1_TBODY1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1"
doe_TD = {"tagName": "TD", "title": "Doe"}
froglogic_Addressbook_BrowserTab = {"title": "froglogic Addressbook", "type": "BrowserTab"}
froglogic_Addressbook_Save_button = {"container": froglogic_Addressbook_BrowserTab, "innerText": "Save", "tagName": "BUTTON", "type": "button"}
froglogic_Addressbook_addButton_button = {"container": froglogic_Addressbook_BrowserTab, "id": "addButton", "tagName": "INPUT", "type": "button", "value": "Add"}
froglogic_Addressbook_emailEdit_text = {"container": froglogic_Addressbook_BrowserTab, "form": "oneitem", "id": "emailEdit", "tagName": "INPUT", "type": "text"}
froglogic_Addressbook_forenameEdit_text = {"container": froglogic_Addressbook_BrowserTab, "form": "oneitem", "id": "forenameEdit", "tagName": "INPUT", "type": "text"}
froglogic_Addressbook_newButton_button = {"container": froglogic_Addressbook_BrowserTab, "id": "newButton", "tagName": "INPUT", "type": "button", "value": "New"}
froglogic_Addressbook_phoneEdit_text = {"container": froglogic_Addressbook_BrowserTab, "form": "oneitem", "id": "phoneEdit", "tagName": "INPUT", "type": "text"}
froglogic_Addressbook_surnameEdit_text = {"container": froglogic_Addressbook_BrowserTab, "form": "oneitem", "id": "surnameEdit", "tagName": "INPUT", "type": "text"}
gview_addresses_DIV = {"id": "gview_addresses", "tagName": "DIV"}
