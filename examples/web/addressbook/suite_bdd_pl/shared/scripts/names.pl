package Names;

use utf8;
use strict;
use warnings;
use Squish::ObjectMapHelper::ObjectName;

# See the chapter 'Script-Based Object Map API' in the Squish manual for
# documentation of the functionality offered by the 'ObjectMapHelper' module.
#
our $confirmpopup = "ConfirmPopup";
our $document = "DOCUMENT";
our $document_html1_body1_div1_div2_div3_div3_div1_table1_tbody1 = "DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1";
our $froglogic_addressbook_browsertab = {"title" => "froglogic Addressbook", "type" => "BrowserTab"};
our $froglogic_addressbook_save_button = {"container" => $froglogic_addressbook_browsertab, "innerText" => "Save", "tagName" => "BUTTON", "type" => "button"};
our $froglogic_addressbook_addbutton_button = {"container" => $froglogic_addressbook_browsertab, "id" => "addButton", "tagName" => "INPUT", "type" => "button", "value" => "Add"};
our $froglogic_addressbook_emailedit_text = {"container" => $froglogic_addressbook_browsertab, "form" => "oneitem", "id" => "emailEdit", "tagName" => "INPUT", "type" => "text"};
our $froglogic_addressbook_forenameedit_text = {"container" => $froglogic_addressbook_browsertab, "form" => "oneitem", "id" => "forenameEdit", "tagName" => "INPUT", "type" => "text"};
our $froglogic_addressbook_newbutton_button = {"container" => $froglogic_addressbook_browsertab, "id" => "newButton", "tagName" => "INPUT", "type" => "button", "value" => "New"};
our $froglogic_addressbook_phoneedit_text = {"container" => $froglogic_addressbook_browsertab, "form" => "oneitem", "id" => "phoneEdit", "tagName" => "INPUT", "type" => "text"};
our $froglogic_addressbook_surnameedit_text = {"container" => $froglogic_addressbook_browsertab, "form" => "oneitem", "id" => "surnameEdit", "tagName" => "INPUT", "type" => "text"};
1;
