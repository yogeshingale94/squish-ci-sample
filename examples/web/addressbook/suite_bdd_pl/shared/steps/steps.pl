require 'names.pl';

use warnings;

package main;

Given("addressbook application is running", sub {
    my $context = shift;
    startBrowser("http://127.0.0.1:9090/AddressBook.html");
    test::compare(waitForObjectExists($Names::document)->title, "froglogic Addressbook");
});

When("I create a new addressbook", sub {
    my $context = shift;
    clickButton(waitForObject($Names::froglogic_addressbook_newbutton_button));
    snooze(2);
    closeConfirm($Names::confirmpopup, 1);
});

Then("addressbook should have zero entries", sub {
    my $context = shift;
    test::compare(waitForObjectExists($Names::document_html1_body1_div1_div2_div3_div3_div1_table1_tbody1)->numChildren, 1);
});

When("I add a new person '|word|','|word|','|any|','|integer|' to address book", sub {
    my $context = shift;
    my ($forename, $surname, $email, $phone) = @_;
    clickButton(waitForObject($Names::froglogic_addressbook_addbutton_button));
    setText(waitForObject($Names::froglogic_addressbook_forenameedit_text), $forename);
    setText(waitForObject($Names::froglogic_addressbook_surnameedit_text), $surname);
    setText(waitForObject($Names::froglogic_addressbook_emailedit_text), $email);
    setText(waitForObject($Names::froglogic_addressbook_phoneedit_text), $phone);
    clickButton(waitForObject($Names::froglogic_addressbook_save_button));
    # save userData for last step
    $context->{userData}{'forename'} = $forename;
    $context->{userData}{'surname'} = $surname;
});

Then("'|integer|' entries should be present", sub {
    my $context = shift;
    my $entries = shift;
    test::compare(waitForObjectExists($Names::document_html1_body1_div1_div2_div3_div3_div1_table1_tbody1)->numChildren, $entries + 1);
});

When("I add new persons to address book", sub {
    my %context = %{shift()};
    my @table = @{$context{'table'}};

    # Drop initial row with column headers
    shift(@table);

    for my $row (@table) {
        my ($forename, $surname, $email, $phone) = @{$row};
        clickButton(waitForObject($Names::froglogic_addressbook_addbutton_button));
        setText(waitForObject($Names::froglogic_addressbook_forenameedit_text), $forename);
        setText(waitForObject($Names::froglogic_addressbook_surnameedit_text), $surname);
        setText(waitForObject($Names::froglogic_addressbook_emailedit_text), $email);
        setText(waitForObject($Names::froglogic_addressbook_phoneedit_text), $phone);
        clickButton(waitForObject($Names::froglogic_addressbook_save_button));
    }
});

Then("previously entered forename and surname shall be at the top", sub {
    my $context = shift;
    test::compare( waitForObject("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD2")->innerText,
        $context->{userData}{'forename'}, "forename?" );
    test::compare( waitForObject("DOCUMENT.HTML1.BODY1.DIV1.DIV2.DIV3.DIV3.DIV1.TABLE1.TBODY1.TR2.TD3")->innerText,
        $context->{userData}{'surname'}, "surname?" );
});
