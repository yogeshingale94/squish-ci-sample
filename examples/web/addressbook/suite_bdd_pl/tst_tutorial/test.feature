Feature: Filling of addressbook
    As a user I want to fill the addressbook with entries

    Scenario: Initial state of created address book
        Given addressbook application is running
        When I create a new addressbook
        Then addressbook should have zero entries

    Scenario: State after adding one entry
        Given addressbook application is running
        When I create a new addressbook
        And I add a new person 'John','Doe','john@m.com','500600700' to address book
        Then '1' entries should be present

    Scenario: State after adding two entries
        Given addressbook application is running
        When I create a new addressbook
        And I add new persons to address book
            | forename  | surname  | email        | phone   |
            | John      | Smith    | john@m.com   | 1231231 |
            | Alice     | Thomson  | alice@m.com  | 2342342 |
        Then '2' entries should be present

    Scenario: Forename and surname is added to table
        Given addressbook application is running
        When I create a new addressbook
        When I add a new person 'Bob','Doe','Bob@m.com','123321231' to address book
        Then previously entered forename and surname shall be at the top

    Scenario Outline: Adding single entries multiple time
        Given addressbook application is running
        When I create a new addressbook
        And I add a new person '<forename>','<surname>','<email>','<phone>' to address book
        Then '1' entries should be present
        Examples:
            | forename | surname  | email       | phone     |
            | John     | Doe      | john@m.com  | 500600700 |
            | Bob      | Koo      | bob@m.com   | 500600800 |
