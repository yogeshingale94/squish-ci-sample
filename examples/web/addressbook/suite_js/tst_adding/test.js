import * as names from 'names.js';

function main() {
    startBrowser("http://localhost:9090/AddressBook.html");
    confirmPopup(names.newButtonButton);
    test.verify(numberOfRows() == 0);
    var data = new Array(
        new Array("Andy", "Beach", "andy.beach@nowhere.com", "555 123 6786"),
        new Array("Candy", "Deane", "candy.deane@nowhere.com", "555 234 8765"),
        new Array("Ed", "Fernleaf", "ed.fernleaf@nowhere.com", "555 876 4654"));
    for (var row = 0; row < data.length; ++row)
        addNameAndAddress(data[row]);
    test.compare(numberOfRows(), 3);
}

function confirmPopup(button) {
    clickButton(waitForObject(button));
    snooze(1.8);
    closeConfirm(names.confirmPopup, true);
}

function numberOfRows() {
    var table = waitForObject(names.dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1);
    var results = table.evaluateXPath(".//TR[contains(@class, 'jqgrow')]");
    return results.snapshotLength;
}

function addNameAndAddress(oneNameAndAddress) {
    clickButton(waitForObject(names.addButtonButton));
    typeText(waitForObject(names.froglogicAddressbookForenameEditText), oneNameAndAddress[0]);
    typeText(waitForObject(names.froglogicAddressbookSurnameEditText), oneNameAndAddress[1]);
    typeText(waitForObject(names.froglogicAddressbookEmailEditText), oneNameAndAddress[2]);
    typeText(waitForObject(names.froglogicAddressbookPhoneEditText), oneNameAndAddress[3]);
    clickButton(waitForObject(names.saveButton));
}
