import * as names from 'names.js';

function main() {
    startBrowser("http://localhost:9090/AddressBook.html");
    test.compare(numberOfRows(), 125)
    clickButton(waitForObject(names.addButtonButton));
    typeText(waitForObject(names.froglogicAddressbookForenameEditText), "Jane");
    typeText(waitForObject(names.froglogicAddressbookSurnameEditText), "Doe");
    typeText(waitForObject(names.froglogicAddressbookEmailEditText), "jane.doe@nowhere.com");
    typeText(waitForObject(names.froglogicAddressbookPhoneEditText), "123 555 4567");
    clickButton(waitForObject(names.froglogicAddressbookSaveButton));
    test.compare(numberOfRows(), 126)
    clickButton(waitForObject(names.froglogicAddressbookJqgAddresses4Checkbox));
    clickButton(waitForObject(names.froglogicAddressbookEditButtonButton));
    // manually inserted selectAll():
    waitForObject(names.froglogicAddressbookSurnameEditText).selectAll();
    typeText(waitForObject(names.froglogicAddressbookSurnameEditText), "Doe");
    clickButton(waitForObject(names.froglogicAddressbookSaveButton));
    clickButton(waitForObject(names.froglogicAddressbookJqgAddresses2Checkbox));
    clickButton(waitForObject(names.froglogicAddressbookRemoveButtonButton));
    snooze(1.4);
    closeConfirm(names.confirmPopup, true);
    test.compare(numberOfRows(), 125)
    test.compare(waitForObjectExists(names.froglogicAddressbookJaneDoeNowhereComTD).innerText, "jane.doe@nowhere.com");
    test.compare(waitForObjectExists(names.froglogicAddressbook1235554567TD).innerText, "123 555 4567");
    test.compare(waitForObjectExists(names.froglogicAddressbookJaneTD).innerText, "Jane");
    test.compare(waitForObjectExists(names.froglogicAddressbookDoeTD).innerText, "Doe");
}

function numberOfRows() {
    var table = waitForObject(names.dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1);
    var results = table.evaluateXPath(".//TR[contains(@class, 'jqgrow')]");
    return results.snapshotLength;
}
