import * as names from 'names.js';

function main() {
    startBrowser("http://localhost:9090/AddressBook.html");
    confirmPopup(names.newButtonButton);
    test.verify(numberOfRows() == 0);
    var limit = 10;
    var records = testData.dataset("MyAddresses.tsv");
    for (var row = 0; row < records.length; ++row) {
        var record = records[row];
        var forename = testData.field(record, "Forename");
        var surname = testData.field(record, "Surname");
        var email = testData.field(record, "Email");
        var phone = testData.field(record, "Phone");
        addNameAndAddress(new Array(forename, surname, email, phone));
        checkNameAndAddress(record);
        if (row > limit)
            break;
    }
    test.compare(numberOfRows(), row + 1);
}

function checkNameAndAddress(record)
{
    var table = waitForObject(names.dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1);
    var cells = [table.evaluateXPath(".//TR/TD[2]"),
                 table.evaluateXPath(".//TR/TD[3]"),
                 table.evaluateXPath(".//TR/TD[4]"),
                 table.evaluateXPath(".//TR/TD[5]")];
    for (var column = 0; column < testData.fieldNames(record).length;
            ++column) {
        var cell = cells[column].snapshotItem(cells[column].snapshotLength-1).innerText;
        var field = testData.field(record, column);
        test.compare(cell, field);
    }
}
// New addresses are inserted at the end

function confirmPopup(button) {
    clickButton(waitForObject(button));
    snooze(2.5);
    closeConfirm(names.confirmPopup, true);
}

function numberOfRows() {
    var table = waitForObject(names.dOCUMENTHTML1BODY1DIV1DIV2DIV3DIV3DIV1TABLE1);
    var results = table.evaluateXPath(".//TR[contains(@class, 'jqgrow')]");
    return results.snapshotLength;
}

function addNameAndAddress(oneNameAndAddress) {
    clickButton(waitForObject(names.addButtonButton));
    typeText(waitForObject(names.froglogicAddressbookForenameEditText), oneNameAndAddress[0]);
    typeText(waitForObject(names.froglogicAddressbookSurnameEditText), oneNameAndAddress[1]);
    typeText(waitForObject(names.froglogicAddressbookEmailEditText), oneNameAndAddress[2]);
    typeText(waitForObject(names.froglogicAddressbookPhoneEditText), oneNameAndAddress[3]);
    clickButton(waitForObject(names.saveButton));
}
