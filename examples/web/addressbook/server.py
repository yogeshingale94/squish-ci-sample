#!/usr/bin/env python

import sys
import os
import socket

if sys.version_info[0] == 2:
    from BaseHTTPServer import HTTPServer
    from SimpleHTTPServer import SimpleHTTPRequestHandler
else:
    from http.server import HTTPServer, SimpleHTTPRequestHandler


def formatAddress(ip, port):
    if ip == "0.0.0.0":
        return "127.0.0.1:{}".format(port)
    elif ip == "127.0.0.1":
        return "localhost:{}".format(port)
    return "{}:{}".format(socket.gethostbyaddr(ip)[0], port)

# Set the cwd to the files directory
# to allow calling the script from some
# other cwd and still have the right files
# served
os.chdir(os.path.dirname(os.path.abspath(__file__)))

port = 9090
if len(sys.argv) > 1 and sys.argv[1].isdigit():
    port = int(sys.argv[1])
server = HTTPServer(("127.0.0.1", port), SimpleHTTPRequestHandler)
try:
    print ("mini-web-server has been started.\n")
    print ("Use this URL to access it:\n")
    print (" http://{}/AddressBook.html \n".format(formatAddress(*server.server_address)))

    server.serve_forever()
except KeyboardInterrupt:
    print ("\b\bmini-web-server has been stopped.")
