# squish-ci-sample

# Installation
- Download and install squish from https://www.froglogic.com/squish/download/  
- Set below environment variables  (Change them as needed)
```
SQUISH_DIR=/Applications/Squish\ for\ Web\ 7.0.0 (This is the path where squish is installed)
SQUISH_LICENSEKEY_DIR=$HOME/.squish-license (This is the path where squish licensekey is present)
SQUISH_SERVER_PORT=4325
DISPLAY=test
```

# Run Squish suite tests locally
We are going to run Squish test suites for `examples/web/addressbook/suite_js`

## Start squish server on one terminal
```
$SQUISH_DIR/bin/squishserver --port $SQUISH_SERVER_PORT 1>server.log 2>&1
```

## Start sample app on another terminal
```
python3 examples/web/addressbook/server.py 
```

## Config setup
```
$SQUISH_DIR/bin/squishserver --port $SQUISH_SERVER_PORT --config addAUT addressbook examples/web/addressbook/suite_js
```

## Run Squish tests
```
$SQUISH_DIR/bin/squishrunner --port $SQUISH_SERVER_PORT --testsuite examples/web/addressbook/suite_js --reportgen junit,report.xml --reportgen stdout
```

Note:
The bin added in `bin` directory is from Mac OS. You need to install Linux Squish IDE and add the bin folder in this repository then Gitlab CI will start working again.