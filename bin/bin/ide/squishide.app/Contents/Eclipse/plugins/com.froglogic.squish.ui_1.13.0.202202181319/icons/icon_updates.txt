Open
----

* changeexp.png: re-create from original file, from old ide, maybe re-create from onf+checkmark?
* checkmark_green.png: re-create from original file, from old ide
* diff.png: re-create from original, from old ide
* frog128.png
* frog48.png
* insert_vp_obj_properties.png: re-create from original file, where?
* insert_vp_screenshot.png: re-create from original file, where?
* insert_vp_table.png: re-create from original file, where?
* insert_vp_titan.png: re-create from original file, where?
* insertcomment.png: re-create from xcf file
* insertvp.png: re-create from original file, where?
* mainscript.png: re-create from original file, from old ide
* new_lookup_image.png: re-create from original file, where? new_file + memory_view
* new_test_case_from_template.png: re-create from original file, where? 
* new_test_data_file.png: re-create from original file, where? testcase + testdata + new-ovr
* objectmap.png:: re-create from original file, where?
* objectnotfound.png: re-create from original file, cancel.png in jface snippets
* perspective_spy.png: re-create from original file, where?
* pick_object.png: re-create from original file, where?
* quit_aut.png: re-create from original file, where?
* record.png: re-create from original file, where?
* record_disabled.png: re-create from original file, where?
* refresh_delayed.png: re-create from original file, where?, greyed refresh + resource_obj
* refresh_spy.png: re-create from original file, where? refresh + target (but with slightly different color)
* run_tests.png: re-create from original file, where?
* script.png:: re-create from original file, from old ide
* shared.png:: re-create from original file, where?
* stop_recording.png: re-create from original file, where? recording + ??
* test_data_delete_columns.png: re-create from original file, where?
* test_data_delete_rows.png: re-create from original file, where?
* test_data_insert_column.png: re-create from original file, where?
* test_data_insert_row.png: re-create from original file, where?
* test_data_rename_column.png: re-create from original file, where?
* test_suite_settings.png: re-create from original file, where? testsuite + wrench
* testdata.png: re-create from original file, where?
* view_appobjects.png: re-create from original file, where? merge with spy-perspective
* view_globalscripts.png: re-create from script and folder
* view_methods.png: re-create from original file, where?
* view_properties.png: re-create from original file, where?
* view_runnerserverlog.png: re-create from original file, where? document-like + debug-ovr
* view_testresults.png: re-create from original file, where?
* view_testsummary.png: re-create from original file, where?
* zoom_fit.png: re-create from original file, where?
* zoom_in.png: replace with reference/copy to zoomIn from o.e.workbench.texteditor or leave as is?
* zoom_out.png: replace with reference/copy to zoomOut from o.e.workbench.texteditor or leave as is?
* zoom_scale.png: copy from nebula/paperclips once a highres version exists there

* Merge constants of merged icons, ought to save 1 handle per image

Done
----
* add_obj.gif: change to reference to o.e.ui
* image_application.png: copy hi-res from pde plugin
* jump_to_symbolic_name.png: merge with insert_action
* new_test_case_script.png: merge with new_test_case, same icon
* objectfound.png: merge with insertvp, maybe rename to checkmark
* spypick.png: merge with pick_object
* alphab_sort_co.gif: copy from o.e.pde.ui
* clear_co.gif: drop constant, only plugin.xml, from o.e.ui
* close_view.gif: constant, o.e.ui
* delete_obj.gif: plugin.xml&constant, o.e.ui
* detailshorizontal.gif: reference verticalOrientation from o.e.search
* detailsvertical.gif: reference horizontalOrientation from o.e.search
* filter_ps.gif: plugin.xml, o.e.ui
* folder.gif: copy from o.e.ui.ide
* image_based_click.gif: copy from jdoc_hover_edit from o.e.jdt.ui
* o_e_ui_ide_gotoobj_tsk.png: copy gotoobj_tsk from o.e.ui.ide
* insertsp.png: drop, unused
* new_file.gif: copy newfile_wiz from o.e.ui.ide
* new_folder.gif: copy newfolder_wiz from o.e.ui.ide
* pause.gif: reference from o.e.ui.navigator
* quick_fix_error_obj.gif: reference from o.e.ui.editors
* quick_fix_warning_obj.gif: reference from o.e.ui.editors
* refresh.gif: reference from o.e.search
* relaunch.gif: reference runlast_co from o.e.debug.ui
* run.png: reference run_exc from o.e.debug.ui
* run_disabled.png: reference run_exc from o.e.debug.ui
* save_objectdump.gif: reference write_obj from o.e.debug.ui
* scrolllock.gif: reference lock_co from o.e.ui.console
* squish_icon.png: use frog* instead
* warn_tsk.png: reference from o.e.ui
* write_obj.gif: merge with save_objdump
* warn_tsk_gray.gif: re-create from warn_tsk from o.e.ui
* error_tsk_gray.gif: recreate, error_tsk + grayscale
* perspective_testdebug.gif: reuse debug_persp from o.e.debug.ui
* frog16.png
* frog32.png
* frog64.png
* add_existing_folder.gif: recreate from folder and add_obj, replaced with copy of importdir_wiz.png from o.e.ui.ide
* launch_aut.png: re-create from original file, where?, application icon + run?
* new_test_case.png: re-create from original file, where? testcase + new-ovr? Instead use new_testcase.png from jdt?
* testcase.png: re-create from original file, where? use test.png from jdt.junit, moved icon to center of image
* new_test_suite.png: re-create from original file, where?, new-folder + testcase or folder + testcase + new-ovr
* testsuite.png: re-create from original file, where? folder + testcase overlay
* perspective_testmgmt.png: re-create from original file, where? replaced with testsuite.png instead
* new_test_case_bdd.png: re-create from original file, where?, new_test_case+ BDD text, text separately scaled with linear interpolation from 12 to 6 pixel height then pasted to smaller icon
* testcase_bdd.png: re-create from original file, where? testcase + bdd text copied from the scaled temp image
* new_image_lookup.png: remove, unused
* view_testsuites.png: re-create from original file, merged with testsuite.png
* open_test_suite.png: re-create from original file, from testsuite + arrow from o.e.jdt.ui/external_browser
* preview.gif: re-create from original file, reused read_obj
* results_nok.png: re-create from original file, o.e.jdt.junit/ovr16/error_ovr.png
* results_ok.png: re-create from original file, o.e.jdt.junit/ovr16/success_ovr.png
